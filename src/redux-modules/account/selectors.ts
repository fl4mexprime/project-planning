import {ITypedState, RootState} from "../../types/store.ts";
import {Account, Person} from "../../types/account.ts";
import {ErrorMessage} from "../../utils/request/types.ts";

export const selectAccountMeta = (state: RootState): ITypedState<Account> => state.account;
export const selectAccountIsLoading = (state: RootState): boolean => state.account.isLoading;
export const selectAccountError = (state: RootState): ErrorMessage => state.account.error;
export const selectAccountPerson = (state: RootState): Person => state.account.data.person
