import {createSlice} from '@reduxjs/toolkit';
import {ErrorMessage} from "../../utils/request/types.ts";
import {Account} from "../../types/account.ts";
import {ITypedState} from "../../types/store.ts";
import {loginAccount, registerAccount} from "./actions.ts";

const initialState: ITypedState<Account> = {
    data: {} as Account,
    isLoading: false,
    error: {} as ErrorMessage
}

const slice = createSlice({
    name: 'Account',
    initialState,
    reducers: {
        resetAccount: () => initialState
    },
    extraReducers: (builder) =>
        builder
            .addCase(loginAccount.pending, (draft) => {
                draft.isLoading = true;
            })
            .addCase(loginAccount.fulfilled, (draft, action) => {
                draft.isLoading = false;
                draft.data = action.payload;
            })
            .addCase(loginAccount.rejected, (draft, action) => {
                draft.isLoading = false;
                draft.error = action.payload as ErrorMessage;
            })
            .addCase(registerAccount.pending, (draft) => {
                draft.isLoading = true;
            })
            .addCase(registerAccount.fulfilled, (draft, action) => {
                draft.isLoading = false;
                draft.data = action.payload;
            })
            .addCase(registerAccount.rejected, (draft, action) => {
                draft.isLoading = false;
                draft.error = action.payload as ErrorMessage;
            })
});

export const {resetAccount} = slice.actions;

export const accountSlice = slice.reducer;
