import {createAsyncThunk} from '@reduxjs/toolkit';
import {ErrorMessage} from '../../utils/request/types';
import {Account, Credentials, Register, UserToken} from "../../types/account.ts";
import {postLogin, postRegister} from "../../api/account/login/post.ts";
import {getAccount} from "../../api/account/login/get.ts";
import {parseToken, setToken} from "../../utils/token.ts";
import _ from "lodash";

export const loginAccount = createAsyncThunk<Account, Credentials, { rejectValue: ErrorMessage }>(
    'account/loginAccount',
    async (arg, {rejectWithValue}) => {
        const {
            data,
            status
        } = await postLogin(arg);

        if (status && (status >= 300 || status < 200)) {
            return rejectWithValue({
                message: 'unathorized',
                errorCode: '401',
                displayMessage: "Email oder Passwort sind falsch"
            });
        }

        if (!_.isString(data?.token)) {
            return rejectWithValue({
                message: 'token missing',
                errorCode: '500',
                displayMessage: "Es wurde kein Token empfangen"
            });
        }

        // Saves token to local storage
        setToken(data?.token as string)

        const token = parseToken<UserToken>(data?.token as string)

        const {data: account} = await getAccount(token.userIdentifier)

        if (!_.isString(account?.identifier)) {
            return rejectWithValue({
                message: 'Account not found',
                errorCode: '500',
                displayMessage: "Es wurde kein Account gefunden"
            });
        }

        return account as Account;
    }
);

export const registerAccount = createAsyncThunk<Account, Register, { rejectValue: ErrorMessage }>(
    'account/registerAccount',
    async (arg, {rejectWithValue}) => {
        const {
            data,
            status
        } = await postRegister(arg);

        if (status && (status >= 300 || status < 200)) {
            return rejectWithValue({
                message: 'unathorized',
                errorCode: '401',
                displayMessage: "Email oder Passwort sind falsch"
            });
        }

        if (!_.isString(data?.identifier)) {
            return rejectWithValue({
                message: 'Account not found',
                errorCode: '500',
                displayMessage: "Es wurde kein Account gefunden"
            });
        }

        const {data: account} = await getAccount(data?.identifier as string)

        if (!_.isString(account?.identifier)) {
            return rejectWithValue({
                message: 'Account not found',
                errorCode: '500',
                displayMessage: "Es wurde kein Account gefunden"
            });
        }

        return account as Account;
    }
);
