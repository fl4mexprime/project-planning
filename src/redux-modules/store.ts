import {combineReducers, configureStore, EnhancedStore, Middleware} from '@reduxjs/toolkit';
import {createLogger} from 'redux-logger';
import {accountSlice} from "./account/slice.ts";

export const createStore = (): EnhancedStore => {
    const rootReducer = combineReducers({
        account: accountSlice
    });

    const reduxLogger = createLogger({
        duration: true,
        collapsed: true
    });

    const middlewares: Array<Middleware<never>> = [];
    middlewares.push(reduxLogger);

    return configureStore({
        reducer: rootReducer,
        middleware: (defaultMiddleware) => defaultMiddleware().concat(middlewares),
        // devTools: !IS_PRODUCTION
    });
};

const store = createStore();

export default store;
