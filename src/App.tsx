import './App.css'
import Admin from "./components/views/admin/Admin.tsx";
import User from "./components/views/user/User.tsx";

function App() {
    const adminMode = false;

    return (
        <>
            {adminMode ? <Admin/> : <User/>}
        </>
    )
}

export default App
