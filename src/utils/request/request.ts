import _ from 'lodash';
import {BASE_URL} from '../../constants/urls';
import {Method, RequestOptions, RequestResult} from './types';
import {containsHttpOrHttpsPrefix, getUtcOffsetAsString} from './utils';
import {getToken} from "../token.ts";

export const request = async <Data = null, Body = null>(
    {
        body,
        method = Method.GET,
        url = ''
    }: RequestOptions<Body>): Promise<RequestResult<Data>> => {
    const headers: HeadersInit = {};

    const localStorageToken = getToken()

    if (_.isString(localStorageToken)) {
        headers['Authorization'] = `Bearer ${localStorageToken}`
    }

    headers.offset = getUtcOffsetAsString();

    const requestData: RequestInit = {
        credentials: 'same-origin',
        headers,
        method
    };

    if (method !== 'GET') {
        headers['Content-Type'] = 'application/json';

        if (body) {
            requestData.body = JSON.stringify(body);
        }
    }

    const result: RequestResult<Data> = {
        meta: {
            method,
            url: containsHttpOrHttpsPrefix(url) ? url : url,
            body: requestData.body
        }
    };

    const requestStart: number = Date.now();

    try {
        const response: Response = await fetch(containsHttpOrHttpsPrefix(url) ? url : `${BASE_URL}${url}`, requestData);

        result.requestDuration = Date.now() - requestStart;
        result.status = response.status;

        try {
            const dataString = await response.text();

            if (dataString && dataString.length > 0) {
                // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
                result.data = JSON.parse(dataString);
            }
        } catch (error) {
            if (error) {
                // @ts-expect-error: Type is correct here
                result.error = error;
            }
        }
        if (response.status === 429) {
            const retryAfterHeaderValue = response.headers.get('retry-after');

            if (_.isString(retryAfterHeaderValue)) {
                let parsedRetryAfterValue;

                try {
                    parsedRetryAfterValue = parseInt(retryAfterHeaderValue, 10);

                    if (
                        _.isNumber(parsedRetryAfterValue)
                        && !Number.isNaN(parsedRetryAfterValue)
                    ) {
                        result.retryAfter = parsedRetryAfterValue;
                    }
                } catch (error) {
                    if (error) {
                        // @ts-expect-error: Type is correct here
                        result.error = error;
                    }
                }
            }
        }
    } catch (error) {
        result.requestDuration = Date.now() - requestStart;

        if (error) {
            // @ts-expect-error: Type is correct here
            result.error = error;
        }
    }

    return result;
};
