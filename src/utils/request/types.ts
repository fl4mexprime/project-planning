export interface RequestOptions<Body> {
    body?: Body;
    method: Method;
    url?: string;
}

export enum Method {
    GET = 'GET',
    HEAD = 'HEAD',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE',
    CONNECT = 'CONNECT',
    OPTIONS = 'OPTIONS',
    TRACE = 'TRACE',
    PATCH = 'PATCH'
}

interface Meta {
    body?: BodyInit | null;
    method: string;
    url: string;

    [key: string]: unknown;
}

export interface RequestResult<Data = unknown> {
    data?: Data;
    error?: Error | undefined;
    meta: Meta;
    requestDuration?: number;
    retryAfter?: number;
    status?: number;

    [key: string]: unknown;
}

export interface ApiFunctionResult<T = null, E = null> {
    data?: T;
    error?: Error;
    errorData?: E;
    errorMessage?: string;
    retryAfter?: number;
    status?: number;
}

export type ErrorMessage = {
    message: string
    errorCode: string
    displayMessage: string
};
