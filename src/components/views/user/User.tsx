import {FC, useEffect} from 'react';
import Login from '../../shared/login/login.tsx';

interface UserProps {
    // TODO: Add props
}

const User: FC<UserProps> = () => {
    useEffect(() => {
        // Add dispatch etc.
    }, []);

    return <div style={{width: "100vw"}}>
        <Login/>
    </div>
};

User.displayName = 'User';

export default User;
