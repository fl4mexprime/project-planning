import {FC, useState} from 'react';
import {Button, CircularProgress, Container} from "@mui/material";
import ProjectConfig from "../../project-config/ProjectConfig.tsx";

interface AdminProps {
    // TODO: Add props
}

const Admin: FC<AdminProps> = () => {
    const [loading, setLoading] = useState(false);
    const [showCreateProject, setShowCreateProject] = useState(false);

    const handleLoadingMocking = () => {
        setLoading(true)
        setTimeout(() => {
            setLoading(false)
            setShowCreateProject(true)
        }, 3500)
    }

    return <>
        {
            loading ? <CircularProgress/> : null
        }
        {
            !loading && !showCreateProject ? <>Sieht hier noch ziemlich leer aus...
                <Container sx={{paddingTop: "10px"}}>
                    <Button onClick={handleLoadingMocking} sx={{border: 1}}>Projekt anlegen</Button>
                </Container></> : null
        }
        {
            showCreateProject ? <ProjectConfig/> : null
        }
    </>
};

Admin.displayName = 'Admin';

export default Admin;
