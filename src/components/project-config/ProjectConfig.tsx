import {FC, useState} from 'react';
import {Button, Container, TextField} from "@mui/material";
import {DatePicker, LocalizationProvider} from "@mui/x-date-pickers";
import {AdapterDateFns} from "@mui/x-date-pickers/AdapterDateFns";
import {de} from "date-fns/locale";

interface ProjectConfigProps {
    // TODO: Add props
}

const ProjectConfig: FC<ProjectConfigProps> = () => {
    const [startDate, setStartDate] = useState<Date>(new Date());
    const [endDate, setEndDate] = useState<Date>();

    const handleStartDateSet = (date: Date | null) => {
        if (date === null) return
        setStartDate(date)
    }

    const handleEndDateSet = (date: Date | null) => {
        if (date === null) return
        setEndDate(date)
    }

    const handleProjectSave = () => {

    }

    return <div style={{width: "450px", boxSizing: "border-box"}}>
        <TextField
            sx={{width: "100%", paddingBottom: "10px"}}
            id="outlined-basic"
            required
            label="Projekt"
            variant="outlined"
        />
        <br/>
        <TextField
            sx={{width: "100%", paddingBottom: "10px"}}
            multiline
            rows={2}
            label="Gibt dem Projekt eine kurze Beschreibung"
            variant="outlined"
        />
        <br/>
        <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={de}>
            <div style={{display: 'flex', justifyContent: 'space-between', gap: "10px"}}>
                <DatePicker
                    sx={{flexGrow: 1}}
                    onChange={(date) => handleStartDateSet(date)}
                    minDate={new Date()}
                    label="Start"
                />
                <DatePicker
                    sx={{flexGrow: 1}}
                    onChange={(date) => handleEndDateSet(date)}
                    minDate={startDate}
                    label="Ende"
                />
            </div>
        </LocalizationProvider>
        <Container sx={{paddingTop: "20px"}}>
            <Button onClick={handleProjectSave} variant={"outlined"}>Speichern</Button>
        </Container>
    </div>
};

ProjectConfig.displayName = 'ProjectConfig';

export default ProjectConfig;
