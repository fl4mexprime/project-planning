import {FC, useEffect} from 'react';
import {AppBar, Box, Button, Toolbar} from "@mui/material";

interface NavBarProps {
    // TODO: Add props
}

const NavBar: FC<NavBarProps> = () => {
    useEffect(() => {
        // Add dispatch etc.
    }, []);

    return <AppBar position={"sticky"} component="nav">
        <Toolbar>
            <Box sx={{display: {xs: 'none', sm: 'block'}}}>
                <Button sx={{color: '#fff'}}>
                    Projekte
                </Button>
                <Button sx={{color: '#fff'}}>
                    Personen
                </Button>
                <Button sx={{color: '#fff'}}>
                    Personen
                </Button>
            </Box>
        </Toolbar>
    </AppBar>
};

NavBar.displayName = 'nav-bar';

export default NavBar;
