import {FC} from 'react';
import "./project-overview.scss"
import {Project} from "../../../types/project.ts";
import ProjectItem from "./project-item/ProjectItem.tsx";

interface ProjectOverviewProps {
    projects: Array<Project>
}

const ProjectOverview: FC<ProjectOverviewProps> = ({projects}) => {


    return <>
        <h1>Projekte</h1>
        <div className="project-overview--project-container">
            {
                projects.map((project) => <ProjectItem project={project} key={project.identifier}/>)
            }
        </div>
    </>
};

ProjectOverview.displayName = 'ProjectOverview';

export default ProjectOverview;
