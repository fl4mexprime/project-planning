import {FC, useState} from 'react';
import {Project} from "../../../../types/project.ts";
import "./project-item.scss"
import "gantt-task-react/dist/index.css";
import {Gantt, Task} from "gantt-task-react";
import clsx from "clsx";
import {
    Timeline,
    TimelineConnector,
    TimelineContent,
    TimelineDot,
    TimelineItem,
    timelineItemClasses,
    TimelineSeparator
} from "@mui/lab";
import CloseIcon from "@mui/icons-material/close";
import {Chip, Fab, Link} from "@mui/material";

interface ProjectItemProps {
    project: Project
}

const ProjectItem: FC<ProjectItemProps> = ({project}) => {
    const [buttonVisible, setButtonVisible] = useState(false);
    const [isExpandedView, setIsExpandedView] = useState(false);

    const handleShowButton = () => setButtonVisible(true);
    const handleHideButton = () => setButtonVisible(false);
    const handleSetExpandedView = () => setIsExpandedView(true)
    const handleUnsetExpandedView = () => setIsExpandedView(false)

    let tasks: Task[] = [
        {
            start: new Date(2020, 1, 1),
            end: new Date(2020, 1, 2),
            name: 'Idea',
            id: 'Task 0',
            type: 'task',
            progress: 45,
            isDisabled: true,
            styles: {progressColor: '#ffbb54', progressSelectedColor: '#ff9e0d'},
        },
    ];

    const {description, title, startDate} = project;

    return <div
        className={clsx({
            "project-item--project": !isExpandedView,
            "project-item--project-expanded": isExpandedView
        })}
        onMouseEnter={handleShowButton}
        onMouseLeave={handleHideButton}
    >
        {
            isExpandedView ?
                <div
                    style={{
                        position: "absolute",
                        right: "2vw",
                        top: "2hv"
                    }}
                >
                    <Fab
                        size={"small"}
                        onClick={handleUnsetExpandedView}
                    >
                        <CloseIcon/>
                    </Fab>
                </div> : null
        }
        <div
            className={clsx({
                "project-item--headline": !isExpandedView,
                "project-item--headline-large": isExpandedView
            })}
        >
            {title}
        </div>

        <div
            className={clsx("project-item--body", {
                fade: !isExpandedView
            })}
        >
            {
                isExpandedView ? <Gantt listCellWidth={""} tasks={tasks}/> : null
            }

            <div className="project-item--description">{description}</div>

            {
                !isExpandedView ?
                    <div className="project-item--timeline">
                        <Timeline
                            sx={{
                                [`& .${timelineItemClasses.root}:before`]: {
                                    flex: 0,
                                    padding: 0,
                                },
                            }}
                        >
                            <TimelineItem>
                                <TimelineSeparator>
                                    <TimelineDot/>
                                    <TimelineConnector/>
                                </TimelineSeparator>
                                <TimelineContent>Planung</TimelineContent>
                            </TimelineItem>
                            <TimelineItem>
                                <TimelineSeparator>
                                    <TimelineDot/>
                                    <TimelineConnector/>
                                </TimelineSeparator>
                                <TimelineContent>Bearbeitung</TimelineContent>
                            </TimelineItem>
                            <TimelineItem>
                                <TimelineSeparator>
                                    <TimelineConnector/>
                                    <TimelineDot/>
                                </TimelineSeparator>
                                <TimelineContent></TimelineContent>
                            </TimelineItem>
                        </Timeline>
                    </div> : null
            }
        </div>

        <div className="project-item--tech-stack">
            <div className={clsx("chip", {
                fade: buttonVisible && !isExpandedView
            })}
            >
                <Chip
                    size="small"
                    label="Typescript"
                    variant="outlined"
                    color="primary"
                />
                <Chip
                    size="small"
                    label="React"
                    variant="outlined"
                    color="secondary"
                />
            </div>
            {
                !isExpandedView ? <div className={clsx("button", {
                    visible: buttonVisible
                })}>
                    <Link
                        onClick={handleSetExpandedView}
                        color={"white"} href="#"
                        underline="none">
                        Mehr anzeigen
                    </Link>
                </div> : null
            }
        </div>
    </div>
};

ProjectItem.displayName = 'ProjectItem';

export default ProjectItem;
