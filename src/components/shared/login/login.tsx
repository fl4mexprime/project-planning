import {FC, useState} from 'react';
import {Button, Link} from "@mui/material";
import Input from '@mui/joy/Input';
import useAppSelector from "../../../hooks/useAppSelector.ts";
import {selectAccountMeta} from "../../../redux-modules/account/selectors.ts";
import useAppDispatch from "../../../hooks/useAppDispatch.ts";
import {loginAccount} from "../../../redux-modules/account/actions.ts";
import "./login.scss"
import CircularProgress from '@mui/joy/CircularProgress';
import {CardType, ContentCard} from "../content-card/content-card.tsx";
import _ from 'lodash';

const login: FC = () => {
    const dispatch = useAppDispatch();
    const {
        isLoading,
        data,
        error
    } = useAppSelector(selectAccountMeta)

    const [emailValue, setEmailValue] = useState<string>();
    const [passwordValue, setPasswordValue] = useState<string>();

    const handleLogin = () => {
        if (!emailValue || !passwordValue) return

        dispatch(loginAccount({
            email: emailValue,
            password: passwordValue
        }))
    }

    console.log(data)

    return (
        <>
            <ContentCard
                show={_.isString(error.errorCode) && !_.isString(data.identifier)}
                message={error.displayMessage} type={CardType.WARNING}
            />
            <div className='login-dialog--container'>
                <div className='login-dialog--container--inner'>
                    {
                        data.identifier ?
                            <div style={{marginBottom: "2em"}}>
                                {`Willkommen zurück, ${data.person.firstName}!`}
                                <br/>
                                <CircularProgress
                                    sx={{marginTop: "1em"}}
                                    determinate={false}
                                    size="sm"
                                    variant="soft"
                                />
                            </div> : null
                    }
                    {
                        !data.identifier ?
                            <>
                                <Input
                                    onChange={(event) => setEmailValue(event.target.value)}
                                    className='input--email'
                                    variant="outlined"
                                    placeholder="Email"
                                    type="email"
                                    disabled={isLoading}
                                />
                                <Input
                                    onChange={(event) => setPasswordValue(event.target.value)}
                                    className='input--password'
                                    variant="outlined"
                                    placeholder="Passwort"
                                    type='password'
                                    disabled={isLoading}
                                />
                                <Button
                                    disabled={!passwordValue || !emailValue || isLoading}
                                    onClick={handleLogin}
                                    className='input--button'
                                    variant="contained">
                                    {isLoading ? <CircularProgress
                                        determinate={false}
                                        size="sm"
                                        variant="soft"
                                    /> : "Login"}
                                </Button>
                                Noch keinen Account? <Link>Jetzt registrieren</Link>
                            </> : null
                    }
                </div>
            </div>
        </>
    )
};

login.displayName = 'login';

export default login;
