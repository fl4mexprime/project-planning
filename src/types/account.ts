export type Account = {
    identifier: string,
    creationTime: string;
    email: string,
    person: Person
}

export type Person = {
    identifier: string;
    creationTime: string;
    firstName: string;
    lastName: string;
    birthDay: string;
}

export type Credentials = {
    email: string
    password: string
}

export type Register = {
    email: Credentials['email'],
    password: Credentials['password'],
    firstName: Person['firstName'],
    lastName: Person['lastName'],
    birthDay: Date;
}

export type RegisterResponse = {
    identifier: Account['identifier']
}

export type LoginResponse = {
    token: string
}

export type UserToken = {
    userIdentifier: Account["identifier"],
    nbf: number,
    exp: number,
    iat: number,
    iss: string,
    aud: string
}
