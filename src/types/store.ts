import {EntityId} from '@reduxjs/toolkit';
import {ErrorMessage} from '../utils/request/types';
import {Account} from "./account.ts";

export interface IEntityAdapterState<T, E = ErrorMessage> {
    ids: Array<EntityId>
    entities: Array<T>
    error: E
    isLoading: boolean,
}

export interface ITypedState<T, E = ErrorMessage> {
    data: T
    isLoading: boolean,
    error: E
}

export type RootState = {
    account: ITypedState<Account>
};
