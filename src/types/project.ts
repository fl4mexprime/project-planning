export type Project = {
    identifier: string,
    title: string,
    description: string,
    startDate: Date,
    endDate?: Date,
    deadLine?: Date
}
