import {Account} from "../../../types/account.ts";
import {request} from "../../../utils/request/request.ts";
import {ApiFunctionResult, Method} from "../../../utils/request/types.ts";

export const getAccount = async (identifier: Account['identifier']): Promise<ApiFunctionResult<Account, Account['identifier']>> =>
    request<Account, Account['identifier']>({
        method: Method.GET,
        url: `/account-api/account/${identifier}`
    });
