import {Credentials, LoginResponse, Register, RegisterResponse} from "../../../types/account.ts";
import {request} from "../../../utils/request/request.ts";
import {ApiFunctionResult, Method} from "../../../utils/request/types.ts";

export const postLogin = async (credentials: Credentials): Promise<ApiFunctionResult<LoginResponse, Credentials>> =>
    request<LoginResponse, Credentials>({
        method: Method.POST,
        url: `/account-api/login`,
        body: credentials
    });

export const postRegister = async (registration: Register): Promise<ApiFunctionResult<RegisterResponse, Register>> =>
    request<RegisterResponse, Register>({
        method: Method.POST,
        url: `/account-api/register`,
        body: registration
    });
